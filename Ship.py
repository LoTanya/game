
class WarShip1():
    x = []
    y = []

    extXMin  = 0
    extXMax = 0
    extYMin = 0
    extYMax = 0
    isWounded = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y
        if min(self.x) - 1 >= 0 :
            self.extXMin = min(self.x) - 1
        else:
            self.extXMin = min(self.x)

        if max(self.x) + 1 < 10 :
            self.extXMax = max(self.x) + 1
        else:
            self.extXMax = max(self.x)

        if min(self.y) - 1 >= 0:
            self.extYMin = min(self.y) - 1
        else:
            self.extYMin = min(self.y)

        if max(self.y) + 1 < 10:
            self.extYMax = max(self.y) + 1
        else:
            self.extYMax = max(self.y)
            
            

    def isValidBuild(self, x, y):
        for i in zip(x,y):
            if i in zip(self.x,self.y):
                return False
        return True
    
    
    def isFired(self, x, y):
        if x in self.x and y in self.y:
            self.x.remove(x)
            self.y.remove(y)
            self.isWounded = 1
        return self.isWounded

    def isDead(self):
        if len(self.x) > 0:
            return False
        else:
            return True
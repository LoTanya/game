from random import randint


board = []

for x in range(2):
    board.append(["O"] * 2)

def print_board(board):
    for row in board:
        print(" ".join(row))

print("Старт")
print_board(board)

def random_row(board):
    return randint(0, len(board) - 1)

def random_col(board):
    return randint(0, len(board[0]) - 1)

ship_row = random_row(board)
ship_col = random_col(board)

for turn in range(2):
    guess_row = int(input("Строка:"))
    guess_col = int(input("Столбец:"))


    if guess_row == ship_row and guess_col == ship_col:
        print("ВЫ ВЫИГРАЛИ!!1!")
        break
    
    else:
   
        if (guess_row < 0 or guess_row > 4) or (guess_col < 0 or guess_col > 4):
            print("")
                
        elif(board[guess_row][guess_col] == "X"):
            print("")
        
        else:
            print("")
            board[guess_row][guess_col] = "X"
        
        print("" + str(turn+1) + "") 
        print_board(board)

if turn >= 1:
    print("ВЫ ПРОИГРАЛИ!!!1")
